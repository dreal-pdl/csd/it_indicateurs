#permet d'afficher bouton de telechargement sur les cartes
jsfile <- "https://rawgit.com/rowanwins/leaflet-easyPrint/gh-pages/dist/bundle.js"

fluidPage(
  fluidRow(
    width = 12,
    column(
      width = 12, 
      includeMarkdown("entete.md")
      )
    ),
  
  useShinydashboard(),
  
  mainPanel(
    width=12,
    tags$head(
      tags$script(src = jsfile),  #pour permettre le bouton de telechargement dans les cartes leaflet
      tags$link(rel = "stylesheet", type = "text/css", href = "style.css"),
      tags$style(HTML("
      .leaflet-container { background: #fff; }
      body {background-color: #ffffff; }
                      ")
                 )
      ),
    
    # Contenu de chacun des onglets
    
    # PAGE INDICATEURS ---------------
    tabsetPanel(
      id = "mon_panel1",
      tabPanel(
        id = "indicateurs", 
        title = "Indicateurs", 
        role = "tabpanel",
        tags$h1("Visualiser un indicateur", align = "center"),
        
        fluidRow( # ouverture de fluidRow de sélection et de comparaison
          box(
            width = 12, 
            status = "primary",
            # sélection du thème, de l'indicateur et de l'année ----------------
            box(
              width = 5, 
              status = "info",
              tags$h5(
                "S\u00e9lectionner une th\u00e9matique, une sous-th\u00e9matique ou une source avant d\'acc\u00e9der \u00e0 la liste des indicateurs", 
                align="left"
                ),
              fluidRow(
                column(
                  width = 6,
                  selectInput(
                    inputId = "select_thematique",
                    label ="th\u00e9matique",
                    choices = c("Toutes", unique(base_indic$thematique) %>% sort())
                    )
                  ),
                column(
                  width = 6,
                  selectInput(
                    inputId = "select_sous_thematique",
                    label = "sous-th\u00e9matique",
                    choices = unique(base_indic$sous_thematique) %>% sort(),
                    selected =  "Toutes"
                    )
                  )
                ),
              fluidRow(
                column(
                  width = 6,
                  selectInput(
                    inputId = "select_source",
                    label = "source",
                    choices = c("Toutes", base_indic$source_libelle %>% unique() %>% sort())
                    )
                  ),
                column(
                  width = 6,
                  selectInput(
                    inputId = "select_indicateur",
                    label = "indicateur",
                    choices = NULL
                    )
                  )
                ),
              fluidRow(
                column(
                  width = 6,
                  actionButton(
                    inputId = "submit",
                    label = "Valider mon indicateur",
                    class = "btn btn-primary"
                    )
                  ),
                column(
                  width = 6,
                  actionButton(
                    inputId = "reset",
                    label = "Remettre \u00e0 z\u00e9ro",
                    class = "btn btn-primary"
                    )
                  )
                ),
              br(),
              fluidRow(
                column(
                  width = 4,
                  selectInput(
                    inputId = "selection_date",
                    label = "mill\u00e9sime",
                    choices = NULL
                    )
                  )
                )
              ),
            # sélection du territoire d'analyse et du territoire de comparaison 
            box(
              width=7, 
              status="info",
              tabBox(
                type="tabs",
                width = 12,
                ## sélection du territoire d'analyse ----------
                tabPanel(
                  "S\u00e9lectionner un territoire", 
                  column(
                    width = 5,
                    ### sélection de la maille -----------------
                    radioButtons(
                      inputId = "select_TypeZone",
                      label = "choisir une maille d\'analyse :",
                      choices = list("D\u00e9partements","Epci","Communes"),
                      selected = "Epci",
                      inline = FALSE
                      ),
                    br(),
                    ### sélection par menu déroulant -----------
                    selectizeInput(
                      inputId = "territoireetude",
                      label = "Territoire",
                      choices = liste_EPCI %>% sort(),
                      options = list(maxOptions = 2000)
                      ),
                    actionButton(
                      inputId = "submit_terr",
                      label = "Valider mon territoire",
                      class = "btn btn-primary"
                      )
                    ),
                  ### sélection par clic sur une carte -----------
                  column(
                    width = 7,
                    leafletOutput("map", height = "200px")
                    )
                  ), 
                ## sélection du territoire de comparaison -------------
                tabPanel(
                  "S\u00e9lectionner un territoire de comparaison", # ouverture de la box de comparaison
                  column(
                    width = 5,
                    ### sélection de la maille -----------------
                    radioButtons(
                      inputId = "select_TypeZone_compare",
                      label = "choisir un territoire de comparaison :",
                      choices = list("R\u00e9gions","D\u00e9partements","Epci","Communes"),
                      selected = "R\u00e9gions",
                      inline = FALSE
                      ),
                    br(),
                    ### sélection par menu déroulant -----------
                    selectizeInput(
                      inputId = "territoireetude_compare",
                      label = "choisir un territoire :",
                      choices = liste_REG,
                      options = list(maxOptions = 2000)
                      ),
                    actionButton(
                      inputId = "submit_terr_comp",
                      label = "Valider mon territoire de comparaison",
                      class = "btn btn-primary"
                      )
                    ),
                  ### sélection par clic sur une carte -----------
                  column(
                    width = 7,
                    leafletOutput("map_compare", height = "200px")
                    )
                  ) # fin du panneau de comparaison
                ) 
              )
            )
          ), # fin de fluidRow de sélection et de comparaison
        
        ## visualisations ------------
        fluidRow( # ouverture de fluidRow des visualisations
          
          ### carte de l'indicateur ----------------
          column(
            width = 7,
            fluidRow(
              box(
                width = 12, 
                status = "primary",
                height = 870,
                leafletOutput('carte_indicateurs', height = 800)
                )
              )
            ),
          column(
            width = 5,
            ### infobox ----------
            box(
              width = 6, 
              status = "primary",
              tags$h5(
                textOutput("titre_chiffre_clef")
                ),
              valueBox(
                value = tags$div(
                  textOutput("chiffre_clef"),
                  style = "font-size:50%;"
                  ),
                subtitle = tags$div(
                  textOutput("date_chiffre_clef"),
                  style = "font-size:90%;"
                  ),
                color = "blue",
                width = 12
                )
              ),
            ### graphiques d'évolution et de comparaison (base 100)  -------------
            box(
              type = "tabs",
              width = 12, 
              status = "primary",
              tabPanel(
                "\u00e9volution",
                highchartOutput("graphique_indicateur")
                ),
              tabPanel(
                "comparaison",
                highchartOutput("graphique_compare_indicateur")
                )
              ),
            ### graphe de ventilation éventuelle par "famille" -----------------
            box(
              width = 12,
              status = "primary",
              selectInput(
                inputId = "selection_famille",
                label = "Répartition de l'indicateur",
                choices = NULL
                ),
              highchartOutput("donut_famille_hc", height = 420)
              )
            )
          ) # fermeture de fluidRow des visualisations
        
        ), # fermeture de tabPanel de la page Indicateurs
      
      # PAGE DE TELECHARGEMENT --------------
      tabPanel(
        id = "telecharger", 
        title = "Télecharger les données", 
        role = "tabpanel",
        fluidRow(
          box(width = 12, 
              status = "primary",
              fluidRow(
                column(
                  width = 4,
                  selectInput(
                    inputId = "telecharger_SelectSource",
                    label = "Source",
                    choices = unique(base_indic$source_libelle) %>% sort()
                    )
                  ),
                column(
                  width = 4,
                  selectizeInput(
                    inputId = "telecharger_SelectIndicateurs",
                    label = "Indicateurs",
                    choices = choices_indicateurs,
                    selected = "Tous",
                    multiple = TRUE,
                    options = list('plugins' = list('remove_button'))
                    )
                  )
                ),
              fluidRow(
                column(
                  width = 4,
                  selectizeInput(
                    inputId = "telecharger_SelectCOG",
                    label = "P\u00e9rimètre g\u00e9ographique",
                    choices = c(liste_REG, liste_DEP, liste_EPCI),
                    multiple = FALSE,
                    options = list('plugins' = list('remove_button'))
                  )
                ),
                column(
                  width = 4,
                  selectizeInput(
                    inputId = "telecharger_SelectMailles",
                    label = "Niveau de d\u00e9tail g\u00e9ographique",
                    choices = choices_mailles,
                    multiple = TRUE,
                    options = list('plugins' = list('remove_button'))
                  )
                )
                ) 
              )
          ),
        fluidRow(
          column(
            width = 4, 
            actionButton(
              inputId = "button",
              label = "valider vos choix",
              class = "btn btn-primary"
              )
            )
          ),
        tags$br(),
        tags$br(),
        fluidRow(
          column(
            width = 4,
            downloadButton(
              outputId = "downloadData",
              label = "T\u00e9l\u00e9charger les donn\u00e9es",
              class = "btn btn-primary"
              )
            )
          ),
        tags$br(),
        tags$br(),
        fluidRow(
          box(
            width=12, 
            status="primary",
            DT::dataTableOutput('tableau')
            )
          )
        ), # fermeture panel téléchargements
      
      # PAGE GUIDE DES INDICATEURS -------------
      tabPanel(
        id = "apropos_indic", 
        title = "Guide des indicateurs",
        role = "tabpanel",
        fluidRow(
          box(
            width = 12,
            status = "primary",
            title = span("Tableau des indicateurs", style = "color:black;font-size:30px"),
            DT::dataTableOutput("tableau_indicateurs")
            ),
          box(
            width = 12, 
            status = "primary",
            title = span("Liste des th\u00e9matiques", style = "color:black;font-size:30px"),
            textOutput("liste_thematique")
            ),
          box(
            width = 12, 
            status = "primary",
            title = span("Liste des sous-th\u00e9matiques", style = "color:black;font-size:30px"),
            textOutput("liste_sous_thematique")
            ),
          box(
            width = 12, 
            status = "primary",
            height = 250,
            title = span("Liste des sources", style = "color:black;font-size:30px"),
            textOutput("liste_source")
            )
          )
        ),  # fermeture panel guide indicateurs
      
      # PAGE GUIDE DES SOURCES ---------
      tabPanel(
        id = "sources",
        title = "Guide des sources",
        role = "tabpanel",
        fluidRow(
          tags$iframe(
            style = "border-width: 0;",
            width = "100%",
            height = 19000,
            # Filename relative to the www/ folder.
            src = basename("guidesources.html")
            )
          )
        ),
      
      # PAGE MENTIONS LEGALES ----------------
      tabPanel(
        id = "mentionslegales", 
        title = "Mentions légales", 
        role = "tabpanel",
        fluidRow(
          box(
            width = 12, 
            status = "primary",
            includeMarkdown("mentionlegale.md")
            )
          )
        )
      
      ) # fermeture de tabsetPanel
  ) # fermeture de mainPanel
  
) # fermeture de fluidpage

