# it-indicateurs
Ce projet vise à créer une application de datavisualisation des données au territoire sur les Pays de la Loire.

Lien vers l'application sur internet : [http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/indicateurs_territoriaux/](http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/indicateurs_territoriaux/)

Lien vers le prototype en cours de développement sur intranet : [http://set-pdl-dataviz.dreal-pdl.ad.e2.rie.gouv.fr/indicateurs_territoriaux_dev/](http://set-pdl-dataviz.dreal-pdl.ad.e2.rie.gouv.fr/indicateurs_territoriaux_dev/)

Les scripts d'alimentation du datamart sont rassemblés dans le projet sgbd_datamart  
https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/sgbd_datamart/
