
# graphique par famille 

creer_hc_bar_famille <- function(.rv = r, selection_date = input$selection_date, selection_famille = input$selection_famille){
  data_donut_famille <- .rv$data_famille %>%
    filter(libelle_famille == selection_famille,
           year(date) == selection_date,
           CodeZone == .rv$selection_terr_zone) %>% 
    arrange(-valeur) %>%
    mutate(libelle_variable = libelle_variable %>% as_factor)
  
  if(data_donut_famille[1,]$unite %in% c("ha", "kWh/degr\u00e9-jour", "ugbta", "unite", "uta")) {
    tooltip <-  "<b>{point.name}</b><br/>{point.valeur:,.0f} {point.unite}"
  } else if(data_donut_famille[1,]$unite %in% c("n")) {
    tooltip <-  "<b>{point.name}</b><br/>{point.valeur:,.0f}"
  } else {
    tooltip <-  "<b>{point.name}</b><br/>{point.valeur:,.1f} {point.unite}"
  }
  
  if(nrow(data_donut_famille) > 0){
    hchart(data_donut_famille,
           type = "bar",
           hcaes(x = libelle_variable, y = valeur)) %>%
      hc_colors(color_base) %>% 
      hc_subtitle(text = paste("Ann\u00e9e", selection_date), align = "left") %>%
      hc_tooltip(headerFormat = '',
                 pointFormat = tooltip) %>%
      hc_xAxis(title = list(text = "")) %>%
      hc_yAxis(title = list(text = ""), labels = list(format  = '{value:,.0f}')) %>%
      hc_exporting(enabled = TRUE)
  }
  
}


