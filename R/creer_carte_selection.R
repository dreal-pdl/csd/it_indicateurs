
# fonction pour menu carto leaflet -------

func_map <- function(type_zone) {
  if (type_zone == "Régions") { 
    leaflet(reg %>% mutate(zone_code = paste0(Zone, " (" ,CodeZone,")")),
            padding = 0, 
            options = leafletOptions(zoomControl = TRUE,minZoom = 5, maxZoom =12)
    ) %>%
      setMaxBounds(lng1 = st_bbox(reg)$xmin %>% as.vector(),
                   lng2 = st_bbox(reg)$xmax %>% as.vector(),
                   lat1 = st_bbox(reg)$ymin %>% as.vector(),
                   lat2 = st_bbox(reg)$ymax %>% as.vector()) %>%
      addPolygons(weight = 1, smoothFactor = 0.2, #color = '#00008B',
                  # fillColor = '#5b937c',
                  fillColor = color_base,
                  color = "white",
                  dashArray = "3",
                  fillOpacity = 0.7,
                  label = ~zone_code,
                  layerId = ~zone_code,
                  highlightOptions = highlightOptions(color = NA,
                                                      fillColor = color_base_fonce,
                                                      weight = 1.5, 
                                                      bringToFront = FALSE, 
                                                      opacity = 1))
  }
  else if (type_zone == "Départements") { 
    leaflet(dep %>% mutate(zone_code = paste0(Zone, " (" ,CodeZone,")")),
            padding = 0, 
            options = leafletOptions(zoomControl = TRUE,minZoom = 5, maxZoom =12)
    ) %>%
      setMaxBounds(lng1 = st_bbox(reg)$xmin %>% as.vector(),
                   lng2 = st_bbox(reg)$xmax %>% as.vector(),
                   lat1 = st_bbox(reg)$ymin %>% as.vector(),
                   lat2 = st_bbox(reg)$ymax %>% as.vector()) %>%
      addPolygons(weight = 1, smoothFactor = 0.2, #color = '#00008B', 
                  fillColor = color_base,
                  color = "white",
                  dashArray = "3",
                  fillOpacity = 0.7,
                  label = ~zone_code,
                  layerId = ~zone_code,
                  highlightOptions = highlightOptions(color = NA,
                                                      fillColor = color_base_fonce,
                                                      weight = 1.5, 
                                                      bringToFront = FALSE, 
                                                      opacity = 1))
  }
  else if(type_zone == "Epci") {
    leaflet(epc %>% mutate(zone_code = paste0(Zone, " (" ,CodeZone,")")),
            padding = 0, 
            options = leafletOptions(zoomControl = TRUE,minZoom = 5, maxZoom =12)
    ) %>%
      setMaxBounds(lng1 = st_bbox(reg)$xmin %>% as.vector(),
                   lng2 = st_bbox(reg)$xmax %>% as.vector(),
                   lat1 = st_bbox(reg)$ymin %>% as.vector(),
                   lat2 = st_bbox(reg)$ymax %>% as.vector()) %>%
      addPolygons(data = dep,
                  weight = 2,
                  opacity = 1,
                  color = "black",
                  fillOpacity = 0) %>% 
      addPolygons(weight = 1, smoothFactor = 0.2, #color = '#00008B', 
                  fillColor = color_base,
                  color = "white",
                  dashArray = "3",
                  fillOpacity = 0.7,
                  label = ~zone_code,
                  layerId = ~zone_code,
                  highlightOptions = highlightOptions(color = NA,
                                                      fillColor = color_base_fonce, 
                                                      weight = 1.5, 
                                                      bringToFront = FALSE,
                                                      opacity = 1))
  }
  else {
    leaflet(com %>% mutate(zone_code = paste0(Zone, " (" ,CodeZone,")")),
            padding = 0, 
            options = leafletOptions(zoomControl = TRUE,minZoom = 5, maxZoom =12) ) %>%
      setMaxBounds(lng1 = st_bbox(reg)$xmin %>% as.vector(),
                   lng2 = st_bbox(reg)$xmax %>% as.vector(),
                   lat1 = st_bbox(reg)$ymin %>% as.vector(),
                   lat2 = st_bbox(reg)$ymax %>% as.vector()) %>%
      addPolygons(data = dep,
                  weight = 2,
                  opacity = 1,
                  color = "black",
                  fillOpacity = 0) %>% 
      addPolygons(weight = 1, smoothFactor = 0.2, #color = '#00008B', 
                  fillColor = color_base,
                  color = "white",
                  dashArray = "3",
                  fillOpacity = 0.7,
                  label = ~zone_code,
                  layerId = ~zone_code,
                  highlightOptions = highlightOptions(color = NA,
                                                      fillColor = color_base_fonce,
                                                      weight = 1.5, 
                                                      bringToFront = FALSE,
                                                      opacity = 1))
  }
}